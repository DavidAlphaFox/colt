;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2017,2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

(define-module (colt config)
  #:use-module (artanis env)
  #:use-module (artanis utils)
  #:use-module (ice-9 pretty-print)
  #:use-module (web uri)
  #:use-module (colt utils)
  #:export (colt-conf-get
            colt-conf-set!
            colt-conf-dump!
            colt-init-config
            current-blog-passwd-db
            regenerate-passwd-db
            check-passwd
            reset-passwd))

(define *default-configs*
  '((blog-author "Colt Blog Engine")
    (github-url "https://gitlab.com/NalaGinrut")
    (blog-name "Colt blog engine")
    (blog-repo "prv/blog")
    (blog-passwd-db "prv/passwd.scm")
    (blog-url "https://nalaginrut.com")
    (admin-url "/admin")
    (blog-author-email "unknown@unknown.com")
    (disqus "fill your disqus account")
    (disqus-pubkey "fill your disqus public token")))

(define *config-file*
  (format #f "~a/prv/colt.scm" (current-toplevel)))

(define *config* #f)

(define (generate-config-file)
  (call-with-output-file *config-file*
    (lambda (port)
      (pretty-print *default-configs* port)
      (close port)
      *default-configs*)))

;; User's set will overwrite the default.
(define (colt-init-config)
  (define (init-config nl)
    (let ((ht (make-hash-table)))
      (for-each
       (lambda (e)
         (hash-set! ht (car e) (cadr e)))
       *default-configs*)
      (for-each
       (lambda (e)
         (hash-set! ht (car e) (cadr e)))
       nl)
      ht))
  (set! *config*
    (init-config
     (if (file-exists? *config-file*)
         (call-with-input-file *config-file* read)
         (generate-config-file)))))

(define (colt-conf-get k)
  (uri-decode
   (or (hash-ref *config* k)
       (throw 'artanis-err 500 colt-conf-get "Invalid config item `~a'" k))))

(define (colt-conf-set! k v)
  (when (not (hash-ref *config* k))
    (throw 'artanis-err 500 colt-conf-set! "Invalid config item `~a'" k))
  (when (and v (not (string-null? v)))
    (hash-set! *config* k v)))

(define (colt-conf-dump!)
  (let ((fp (open-file *config-file* "w+"))
        (ll (hash-map->list list *config*)))
    (pretty-print ll fp)
    (close fp)))

(define (current-blog-passwd-db)
  (format #f "~a/~a" (current-toplevel)
          (colt-conf-get 'blog-passwd-db)))

(define (regenerate-passwd-db)
  (call-with-output-file (current-blog-passwd-db)
    (lambda (port)
      (write
       `(("username" . "admin") ("password" . "123"))
       port))))

(define (call-with-current-passwd-db proc)
  (when (not (file-exists? (current-blog-passwd-db)))
    (regenerate-passwd-db))
  (let* ((fp (open-file (current-blog-passwd-db) "r+"))
         (ll (read fp)))
    (seek fp 0 SEEK_SET)
    (let ((ret (proc ll fp)))
      (close fp)
      ret)))

(define (check-passwd passwd)
  (call-with-current-passwd-db
   (lambda (ll _)
     (and passwd (string=? (assoc-ref ll "password") passwd)))))

(define (reset-passwd passwd)
  (call-with-current-passwd-db
   (lambda (ll port)
     (write (assoc-set! ll "password" passwd) port))))
