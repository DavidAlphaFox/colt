<title><%=  (colt-conf-get 'blog-name) %></title>
<@free-js-ann %>
<@icon favicon.png %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="language" content="english">
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>
<meta name="referrer" content="strict-origin-when-cross-origin">
<link rel="preload" as="font" href="/pub/css/fonts/OpenSans-Regular.ttf">
<@css materialize.min.css %>
<@css blog.css %>
<@js jquery-3.4.1.min.js %>
<@js materialize.min.js %>
<@js highlight.pack.js %>
<link rel="alternate" type="application/rss+xml" href="/feed/atom" />
<script>
 $(document).ready(function () {
     $('.sidenav').sidenav();
 });
</script>
