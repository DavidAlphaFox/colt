<script>
 hljs.configure({
     tabReplace: '  ', // 2 spaces
 });

 document.querySelectorAll("pre.ql-syntax").forEach(block => {
     hljs.highlightBlock(block);
 });

 /* To be compatible with shjs */
 document.querySelectorAll('pre[class*="sh_"]').forEach(block => {
     hljs.highlightBlock(block);
 });

</script>
