/*
  @licstart  The following is the entire license notice for the
  JavaScript code in this page.

  Copyright (C) 2019  Mu Lei known as NalaGinrut <mulei@gnu.org>

  The JavaScript code in this page is free software: you can
  redistribute it and/or modify it under the terms of the GNU
  General Public License (GNU GPL) as published by the Free Software
  Foundation, either version 3 of the License, or (at your option)
  any later version.  The code is distributed WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

  As additional permission under GNU GPL version 3 section 7, you
  may distribute non-source (e.g., minimized or compacted) forms of
  that code without the copy of the GNU GPL normally required by
  section 4, provided you include this license notice and a URL
  through which recipients can access the Corresponding Source.


  @licend  The above is the entire license notice
  for the JavaScript code in this page.
*/
window.addEventListener('touchstart', {}); // in top window
window.addEventListener('touchmove', {}); // in top window

document.addEventListener('DOMContentLoaded', function() {
    let sidenav = document.querySelectorAll('.sidenav');
    let mboxed = document.querySelectorAll('.materialboxed');
    M.Sidenav.init(sidenav, {});
    M.Materialbox.init(mboxed, {});
});

function try_to_load_page_with_auth(url) {
    fetch(url, { method:'head' })
        .then(response => { return response.status; })
        .then(status => {
            if (401 == status) {
                window.location.pathname = "/dashboard/login";
            } else {
                let dc = document.getElementById('dashboard_sandbox');
                dc.src = url;
            }
        })
        .catch(error => alert(error));
}

function show_upload_button(e){
    let button = document.querySelector('#upload_img_button');
    let image = document.querySelector('#head_pic');
    image.style.transition = '1s';
    image.style.opacity = '.5';
    button.style.transition = '1s';
    button.style.display = 'block';
}

function hide_upload_button(e){
    let button = document.querySelector('#upload_img_button');
    let image = document.querySelector('#head_pic');
    image.style.transition = '1s';
    image.style.opacity = '1';
    button.style.display = 'none';
}

function choose_file() {
    let ub = document.getElementById('upload_button');
    ub.click();
}

function upload_img(obj) {
    if (window.confirm("Upload this image?")) {
        let form_data = new FormData();
        //alert(obj.files[0].name);
        form_data.append('file', obj.files[0], 'head.jpg');
        let options = {
            method: "POST",
            body: form_data
        };

        fetch("/v1/colt/upload_head_img", options)
            .then(response => {
                if (200 == response.status) {
                    let img = document.getElementById('head_pic');
                    img.src = "/pub/img/head.jpg?time="+new Date().getTime();
                    hide_upload_button(this);
                }});

        obj.value = "";
    }
}

function show_new_post_page() {
    try_to_load_page_with_auth("/dashboard/new_post");
}

function show_articles_edit_page() {
    try_to_load_page_with_auth("/dashboard/edit");
}

function resize_iframe(obj) {
    //let height = obj.contentWindow.document.body.scrollHeight;
    obj.style.height = '680px';
    obj.style.width = '100%';
}

function show_comments_page(account) {
    let url = "https://" + account + ".disqus.com/admin/";
    window.location = url;
}

function show_settings_page() {
    try_to_load_page_with_auth("/dashboard/setting");
}

function show_intro_page() {
    try_to_load_page_with_auth("/dashboard/intro");
}

function reset_settings() {
    window.location.href = window.location.href;
}

function save_settings() {
    let form = document.getElementById('setting_form');
    let form_data = new FormData(form);
    let qstr = new URLSearchParams(form_data).toString();

    fetch('/v1/colt/change_setting', {
        method: 'POST',
        body: qstr,
    })
        .then(response => response.json())
        .then(json => {
            if (401 == json.status)
            {
                alert(json.reason);
            }
            else
            {
                alert(json.reason);
                reset_settings();
            }
        })
        .catch(error => console.log(error));
}
