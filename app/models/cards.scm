;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.
(define-module (app models cards)
  #:use-module (app models posts)
  #:use-module ((rnrs) #:select (div-and-mod))
  #:use-module (srfi srfi-11)
  #:export (generate-tips
            generate-articles-status
            generate-tags-status))

(define (latest-post-check)
  (define *date-lst*
    `((year . 29030400)
      (month . 2419200)
      (week . 604800)
      (day . 86400)))
  (define lst '(year month week day))
  (define (gen-time s k) (format #f "~a ~a~p" s k s))
  (define (->friendly-time-hint ts)
    (let lp ((ts ts) (ll lst) (ret '()))
      (if (null? ll)
          (string-join (reverse ret) " ")
          (let-values (((d m) (div-and-mod ts (assoc-ref *date-lst* (car ll)))))
            (cond
             ((zero? d) (lp m (cdr ll) ret))
             (else (lp m (cdr ll) (cons (gen-time d (car ll)) ret))))))))
  (let* ((p (get-the-latest-post))
         (time-range (- (current-time) (string->number (post-timestamp p)))))
    (cond
     ((> time-range (assoc-ref *date-lst* 'day))
      (->friendly-time-hint time-range))
     (else #f))))

(define (about-latest-post time-fact)
  (if time-fact
      (format #f "You haven't posted a new article for <b>~a</b>.~%Keep writing!" time-fact)
      "Seems you have good habit to write everyday!"))

(define (about-all-posts posts-fact)
  (let ((cnt (length posts-fact)))
    (format #f "There're <b>~a</b> post~p in total." cnt cnt)))

(define (about-all-tags tags-fact)
  (define-syntax-rule (->url tag)
    (format #f "<a href='/tags/less/~a' class='tag-link'>~a</a>" tag tag))
  (define (get-hottest tags)
    (if (null? tags)
        "<b>Nothing</b>"
        (->url (caar (list-tail tags (1- (length tags)))))))
  (define (get-poorest tags)
    (if (null? tags)
        "<b>Nothing</b>"
        (->url (caar tags))))
  (let ((tags (get-ordered-tags-list))
        (cnt (count-tags)))
    (format #f "There're <b>~a</b> tag~p. The hottest is ~a, the poorest is ~a."
            cnt cnt (get-hottest tags) (get-poorest tags))))

;; (fact-name fact-check fact-comment)
(define *facts*
  `((latest-post ,latest-post-check ,about-latest-post)
    (posts-status ,get-all-posts ,about-all-posts)
    (tags-status ,get-all-posts ,about-all-tags)))

(define (check-fact key)
  (let ((fact (assoc-ref *facts* key)))
    ((cadr fact) ((car fact)))))

(define-syntax-rule (say-a-fact key)
  (let ((fact (check-fact key)))
    (call-with-output-string
      (lambda (port)
        (for-each
         (lambda (p)
           (format port "<p>~a</p>" p))
         (string-split fact #\np))))))

(define (generate-tips)
  (say-a-fact 'latest-post))

(define (generate-articles-status)
  (say-a-fact 'posts-status))

(define (generate-tags-status)
  (say-a-fact 'tags-status))
