;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2019
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Colt is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Colt is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

(use-modules (colt config))

(define (login-checker username passwd)
  (when (not (file-exists? (current-blog-passwd-db)))
    (regenerate-passwd-db))
  (let* ((pwdb (call-with-input-file (current-blog-passwd-db) read))
         (user (assoc-ref pwdb "username"))
         (pw (assoc-ref pwdb "password")))
    (and (string=? user username)
         (string=? pw passwd))))

(post "/auth"
  #:auth `(post "username" "password" ,login-checker)
  #:session #t
  (lambda (rc)
    (if (or (:session rc 'check) (:auth rc))
        (let ((referer (get-referer rc #:except "/dashboard/login*")))
          (if referer
              (redirect-to rc referer)
              (redirect-to rc "/dashboard")))
        (redirect-to rc "/dashboard/login?failed=true"))))
