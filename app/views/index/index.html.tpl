<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
    </head>

    <body>
        <@include common_nav.html.tpl %>
        <div class="main-content container">
            <%= post-content %>
        </div>
    </body>

</html>
