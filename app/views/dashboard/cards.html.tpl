<!DOCTYPE html>
<html>
    <head>
        <title><%= (current-appname) %></title>
        <@icon favicon.ico %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="language" content="english">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>
        <meta name="referrer" content="same-origin">
        <link rel="preload" as="font" href="/pub/css/fonts/OpenSans-Regular.ttf">
        <@css materialize.min.css %>
        <@js materialize.min.js %>
        <@css blog.css %>
        <@css custom-blue-theme.css %>
        <@css dashboard.css %>
    </head>

    <body style="background-image:url('/pub/img/bg-0.jpg');">
        <div class="row">
            <div id="card_tips" class="col s12 m5">
                <div class="card small blue darken-1 hoverable">
                    <div class="card-content white-text">
                        <span class="card-title">Tips</span>
                        <div class="card-content">
                            <%= (generate-tips) %>
                        </div>
                    </div>
                    <div class="card-action">
                        <a href="/dashboard/new_post" class="btn-flat">Let me write!</a>
                    </div>
                </div>
            </div>

            <div id="card_articles_status" class="col s12 m5">
                <div class="card small blue darken-1 hoverable">
                    <div class="card-content white-text">
                        <span class="card-title">Articles</span>
                        <div class="card-content">
                            <%= (generate-articles-status) %>
                        </div>
                    </div>
                    <div class="card-action">
                        <a href="/dashboard/edit" class="btn-flat">Let me check them!</a>
                    </div>
                </div>
            </div>

            <div id="card_articles_status" class="col s12 m5">
                <div class="card small blue darken-1 hoverable">
                    <div class="card-content white-text">
                        <span class="card-title">Tags</span>
                        <div class="card-content">
                            <%= (generate-tags-status) %>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
