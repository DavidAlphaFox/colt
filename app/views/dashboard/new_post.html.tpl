<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
        <@css monokai-sublime.min.css %>
        <@css quill.snow.css %>
        <@css quill.bubble.css %>
        <@css katex.min.css %>
        <@css editor.css %>
        <@js quill.min.js %>
        <@js katex.min.js %>
        <@js editor.js %>
        <@js image-resize.min.js %>
    </head>

    <body onload="new_post()">
        <div class="main-content container top" style="height:900px;">
            <div class="row">
                <div class="col s12">
                    <div class="row">
                        <div class="input-field col s6">
                            <input placeholder="Enter your title..." id="article-title" type="text" class="validate" name="title">
                            <label for="article-title">Title</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="article-tag" type="text" name="tag" class="validate">
                            <label for="article-tag">Enter your tags...</label>
                        </div>
                        <input id="article-author" value=<%= author %> type="text" name="author" style="visibility:hidden">
                    </div>

                    <div class="row">
                        <script>
                         /* For MaterializeCSS select component */
                         document.addEventListener('DOMContentLoaded', function() {
                             let elems = document.getElementById('publish-status');
                             M.FormSelect.init(elems, {});
                         });
                        </script>
                        <div class="input-field col s6">
                            <select id="publish-status">
                                <option value="" disabled selected><label>Choose draft to not publish</label></option>
                                <option value="publish">publish</option>
                                <option value="draft">draft</option>
                            </select>
                            <label for="article-status">Publish status:</label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="standalone-container">
                <div id="toolbar-container">
                    <span class="ql-formats">
                        <select class="ql-font">
                            <option selected>Open Sans</option>
                        </select>
                        <select class="ql-size"></select>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-bold"></button>
                        <button class="ql-italic"></button>
                        <button class="ql-underline"></button>
                        <button class="ql-strike"></button>
                    </span>
                    <span class="ql-formats">
                        <select class="ql-color"></select>
                        <select class="ql-background"></select>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-script" value="sub"></button>
                        <button class="ql-script" value="super"></button>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-header" value="1"></button>
                        <button class="ql-header" value="2"></button>
                        <button class="ql-blockquote"></button>
                        <button class="ql-code-block"></button>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-list" value="ordered"></button>
                        <button class="ql-list" value="bullet"></button>
                        <button class="ql-indent" value="-1"></button>
                        <button class="ql-indent" value="+1"></button>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-direction" value="rtl"></button>
                        <select class="ql-align"></select>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-link"></button>
                        <button class="ql-image"></button>
                        <button class="ql-video"></button>
                        <button class="ql-formula"></button>
                    </span>
                    <span class="ql-formats">
                        <button class="ql-clean"></button>
                    </span>
                </div>
                <div id="editor-container"></div>
            </div>
            <button type="button" class="btn blue darken-2 article-buttons" onclick="submit_post()" id="submit-button">Submit</button>
        </div>
    </body>
    <@js editor.js %>
    <@include foot.html.tpl %>
</html>
