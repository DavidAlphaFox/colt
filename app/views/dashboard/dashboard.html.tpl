<!DOCTYPE html>
<html>
    <head>
        <title><%= (current-appname) %></title>
        <@icon favicon.ico %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="language" content="english">
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'>
        <meta name="referrer" content="strict-origin-when-cross-origin">
        <link rel="preload" as="font" href="/pub/css/fonts/OpenSans-Regular.ttf">
        <@css materialize.min.css %>
        <@js materialize.min.js %>
        <@css blog.css %>
        <@css dashboard.css %>
        <@css custom-blue-theme.css %>
        <@js dashboard.js %>
    </head>

    <body class="noScroll">
        <div class="right" style="width:81%;">
            <nav class="white" style="border-bottom-color:blue;">
                <div class="nav-wrapper right" style="padding-right: 50px;">
                    <ul id="nav-mobile hide-on-med-and-down">
                        <li><a href="/" class="blue-text">Main page</a></li>
                        <li><a href="dashboard/logout" class="blue-text">Logout</a></li>
                    </ul>
                </div>
            </nav>

            <div id="dashboard_content" class="center">
                <iframe sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-modals"
                        onload="resize_iframe(this)" id="dashboard_sandbox" src="dashboard/cards" referrerpolicy="strict-origin-when-cross-origin"
                        style="border:0">
                </iframe>
            </div>
        </div>

        <div class="sidenav sidenav-fixed" style="width:19%;background-image:url('/pub/img/ink_paint.jpg');">
            <ul id="slide-out">
                <li>
                    <div id="upload_img_area" onmouseover="show_upload_button(this)" onmouseout="hide_upload_button(this)" class="hoverable">
                        <div style="height:0px;overflow:hidden">
                            <input type="file" id="upload_button" onchange="upload_img(this)" accept="image/jpeg">
                        </div>
                        <div id="upload_img_button_background" class="background">
                            <div id="upload_img_button" class="responsive-img" onclick="choose_file()" alt="Upload a new pic">
                            </div>
                            <img id="head_pic" class="responsive-img" src="pub/img/head.jpg">
                        </div>
                    </div>
                </li>
                <div class="card-action">
                    <li><a onclick="show_new_post_page()" class="btn-flat waves-effect waves-blue">New Post</a></li>
                    <li><a onclick="show_articles_edit_page()" class="btn-flat waves-effect waves-blue">Articles</a></li>
                    <li><a onclick='show_comments_page(<%= account %>)' class="btn-flat waves-effect waves-blue">Comments</a></li>
                    <li><a onclick="show_settings_page()" class="btn-flat waves-effect waves-blue">Settings</a></li>
                    <li><a onclick="show_intro_page()" class="btn-flat waves-effect waves-blue">Intro</a></li>
                </div>
            </ul>
        </div>
    </body>
</html>
