<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
        <@css monokai-sublime.min.css %>
        <@css quill.snow.css %>
        <@css quill.bubble.css %>
        <@css katex.min.css %>
        <@css articles_edit.css %>
        <@js quill.min.js %>
        <@js katex.min.js %>
        <@js editor.js %>
        <@js image-resize.min.js %>
    </head>

    <body onload="init_intro_page()">
        <div class="main-content container top" id="articles-content" style="visibility:hidden">
            <%= intro-content %>
        </div>
        <div id="edit-content" class="container">
            <div class="msg-layer-none" id="msg-layer"></div>
            <div class="center"><h3>Introduce yourself</h3></div>
            <div id="toolbar-container">
                <span class="ql-formats">
                    <select class="ql-font">
                        <option selected>Open Sans</option>
                    </select>
                    <select class="ql-size"></select>
                </span>
                <span class="ql-formats">
                    <button class="ql-bold"></button>
                    <button class="ql-italic"></button>
                    <button class="ql-underline"></button>
                    <button class="ql-strike"></button>
                </span>
                <span class="ql-formats">
                    <select class="ql-color"></select>
                    <select class="ql-background"></select>
                </span>
                <span class="ql-formats">
                    <button class="ql-script" value="sub"></button>
                    <button class="ql-script" value="super"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-header" value="1"></button>
                    <button class="ql-header" value="2"></button>
                    <button class="ql-blockquote"></button>
                    <button class="ql-code-block"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-list" value="ordered"></button>
                    <button class="ql-list" value="bullet"></button>
                    <button class="ql-indent" value="-1"></button>
                    <button class="ql-indent" value="+1"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-direction" value="rtl"></button>
                    <select class="ql-align"></select>
                </span>
                <span class="ql-formats">
                    <button class="ql-link"></button>
                    <button class="ql-image"></button>
                    <button class="ql-video"></button>
                    <button class="ql-formula"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-clean"></button>
                </span>
            </div>
            <div id="editor-container" style="height:450px;"></div>
            <button id="submit-button" class="btn blue darken-2 article-buttons" onclick="intro_submit()">Submit</button>
            <button class="btn blue darken-2 article-buttons" onclick="cancel_edit()">Cancel</button>
        </div>
    </body>

    <@include foot.html.tpl %>
</html>
