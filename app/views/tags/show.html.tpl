<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <@include header.html.tpl %>
        <@css article.css %>
    </head>

    <body>
        <@include common_nav.html.tpl %>
        <div class="main-content container">
            <%= content %>
        </div>
    </body>

    <@include foot.html.tpl %>
</html>
