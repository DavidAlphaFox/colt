<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
      xml:base=<%= (object->string blog-url) %>>
    <@include atom-header.html.tpl %>
    <%= entries %>
</feed>
