FROM        registry.gitlab.com/nalaginrut/artanis
MAINTAINER  Mu Lei
ENV         LANG C.UTF-8

ARG CACHEBUST=1
RUN set -ex \
        && git clone --depth 1 https://gitlab.com/NalaGinrut/colt.git
